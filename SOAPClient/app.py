import zeep
from suds.client import Client
import xml.etree.ElementTree as ET
from zeep import helpers
from flask import Flask

app = Flask(__name__)

wsdl_authors = "http://localhost:9998/ws/library/authors?wsdl"
wsdl_books = "http://localhost:9998/ws/library/books?wsdl"


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/authors/<name>')
def get_author_by_name(name):
    client = Client(wsdl_authors)

    print(client.service.getAuthorByName(name))

    return client.service.getAuthorByName(name)


@app.route('/authors')
def get_authors():
    client = Client(wsdl_authors)

    return client.service.getAuthors()


@app.route('/authors/add/<first>/<last>')
def add_author(first, last):
    client = Client(wsdl_authors)

    client.service.insertAuthor(first + "," + last)

    return "Done!"


@app.route('/books')
def get_books():
    client = Client(wsdl_books)

    return client.service.getBooks()


@app.route('/books/<name>')
def get_book_by_name(name):
    client = Client(wsdl_books)

    return client.service.getBookByName(name)


@app.route('/books/add')
def add_book():
    client = Client(wsdl_books)

    client.service.insertBook("1111111111111,Toto a l'ecole,21/01/2012,Jean:Mahmoud")

    return "Book added!"


if __name__ == '__main__':
    app.run()
