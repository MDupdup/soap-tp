package web;

import hibernate.dao.AuthorDAO;
import hibernate.dao.BookDAO;
import hibernate.models.Author;
import hibernate.models.Book;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "web.IBookMiddleware")
public class BookMiddleware implements IBookMiddleware {

    private final BookDAO bookDAO = getBookDAO();


    @Override
    public BookDAO getBookDAO() {
        return new BookDAO();
    }

    @Override
    public String getBookByName(String str) {
        Book book = bookDAO.getOneByName(str);

        return book.getId() + ", " + book.getIsbn() + ", " + book.getTitle() + ", " + book.getAuthor().getFirstName() + " " + book.getAuthor().getLastName();
    }

    @Override
    public String getBooks() {
        List<Book> books = bookDAO.getAll();
        StringBuilder bookString = new StringBuilder();

        for (Book b: books) {
            System.out.println(b.getTitle());
            bookString.append(b.getId() + "," + b.getIsbn() + "," + b.getTitle() + ", " + b.getAuthor().getFirstName() + ":" + b.getAuthor().getLastName() + "\n");
        };


        return bookString.toString();
    }

    @Override
    public void insertBook(String bookStr) {
        AuthorDAO adao = new AuthorDAO();

        String[] parts = bookStr.split(",");

        Author newAuthor = new Author(parts[3].split(":")[0], parts[3].split(":")[1]);

        adao.addOne(newAuthor);
        Book book = new Book(parts[0], parts[1], parts[2], newAuthor);


        bookDAO.addOne(book);
    }

    @Override
    public void deleteBook(Long id) {

    }

    @Override
    public void updateBook(Book book) {

    }
}
