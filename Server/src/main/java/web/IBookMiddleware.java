package web;

import hibernate.dao.AuthorDAO;
import hibernate.dao.BookDAO;
import hibernate.models.Author;
import hibernate.models.Book;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
public interface IBookMiddleware {

    @WebMethod
    BookDAO getBookDAO();

    @WebMethod
    String getBookByName(String str);

    @WebMethod
    String getBooks();

    @WebMethod
    void insertBook(String book);

    @WebMethod
    void deleteBook(Long id);

    @WebMethod
    void updateBook(Book book);
}
