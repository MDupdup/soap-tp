package web;

import hibernate.dao.AuthorDAO;
import hibernate.models.Author;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
public interface IAuthorMiddleware {

    @WebMethod
    AuthorDAO getAuthorDAO();

    @WebMethod
    String getAuthorByName(String str);

    @WebMethod
    String getAuthors();

    @WebMethod
    void insertAuthor(String author);

    @WebMethod
    void deleteAuthor(Long id);

    @WebMethod
    void updateAuthor(Author author);

}
