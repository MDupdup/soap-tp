package web;

import hibernate.dao.AuthorDAO;
import hibernate.dao.BookDAO;
import hibernate.models.Author;
import hibernate.models.Authors;
import hibernate.models.Book;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "web.IAuthorMiddleware")
public class AuthorMiddleware implements IAuthorMiddleware {

    private final AuthorDAO authorDAO = getAuthorDAO();

    @Override
    public AuthorDAO getAuthorDAO() {
        return new AuthorDAO();
    }

    @Override
    public String getAuthorByName(String str) {
        Author author = authorDAO.getOneByName(str);

        return author.getId() + "," + author.getFirstName() + " " + author.getLastName();
    }

    @Override
    public String getAuthors() {
        List<Author> authors = authorDAO.getAll();
        StringBuilder bookString = new StringBuilder();

        for (Author a: authors) {
            System.out.println(a.getFirstName());
            bookString.append(a.getId() + "," + a.getFirstName() + " " + a.getLastName() + "\n");
        };


        return bookString.toString();
    }

    @Override
    public void insertAuthor(String authorStr) {
        AuthorDAO adao = new AuthorDAO();

        String[] parts = authorStr.split(",");

        Author author = new Author(parts[0], parts[1]);

        adao.addOne(author);
    }

    @Override
    public void deleteAuthor(Long id) {

    }

    @Override
    public void updateAuthor(Author author) {

    }
}
