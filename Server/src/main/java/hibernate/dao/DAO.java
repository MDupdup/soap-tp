package hibernate.dao;

import java.util.List;

public interface DAO<T> {

    public T getOneByName(String str);

    public List<T> getAll();

    public void addOne(T obj);

    public void deleteOne(T obj);

    public void updateOne(T obj);
}
