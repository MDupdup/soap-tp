package hibernate.dao;

import hibernate.models.Author;
import hibernate.models.Book;
import hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class AuthorDAO implements DAO<Author> {

    Transaction transaction = null;

    @Override
    public Author getOneByName(String str) {

        Author author = new Author();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("from Author where firstName like '"+ str +"' or lastName like '"+ str +"' or firstName ||' '|| lastName like '"+ str +"'", Author.class);
            author = (Author) query.list().get(0);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        return author;
    }

    @Override
    public List<Author> getAll() {
        List<Author> authors = new ArrayList<>();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            authors = session.createQuery("from Author", Author.class).list();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        return authors;
    }

    @Override
    public void addOne(Author author) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.save(author);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOne(Author author) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.delete(author);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void updateOne(Author author) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.update(author);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
