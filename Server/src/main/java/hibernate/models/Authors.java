package hibernate.models;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "authors")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Authors.class})
public class Authors {

    @XmlElement(name = "author")
    private List<Author> authors = null;

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
