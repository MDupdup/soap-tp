package publish;

import hibernate.models.Author;
import hibernate.models.Book;
import hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import web.AuthorMiddleware;
import web.BookMiddleware;

import javax.xml.ws.Endpoint;


public class HWPublisher {

    public static void main(String[] args) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            Author rowling = new Author("J.K.", "Rowling");
            Author king = new Author("Stephen", "King");
            Author harris = new Author("Thomas", "Harris");
            Author yancey = new Author("Rick", "Yancey");
            Author herbert = new Author("Frank", "Herbert");

            session.save(rowling);
            session.save(king);
            session.save(harris);
            session.save(yancey);
            session.save(herbert);

            session.save(new Book("9780312022822", "Le silence des agneaux", "19/05/1988", king));
            session.save(new Book("9780141345833","La 5e Vague - Tome I", "07/05/2013", yancey));
            session.save(new Book("9780439139601","Harry Potter et la Coupe de Feu", "08/07/2000", rowling));
            session.save(new Book("9780606170970","Harry Potter à l'Ecole des Sorciers", "26/06/1997", rowling));
            session.save(new Book("9780399128967","Dune", "01/08/1965", herbert));


            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        Endpoint.publish("http://localhost:9998/ws/library/authors", new AuthorMiddleware());
        Endpoint.publish("http://localhost:9998/ws/library/books", new BookMiddleware());
    }
}
